"""
Gitlab Project Configurator main module.

The entry point of the command line tools is :py:func:`gpc.cli.main`.
"""
# updated by poetry-dynamic-versioning
__version__ = "0.0.0"


def version():
    """Return a string with the full version of the current ``gpc`` package."""
    return __version__
