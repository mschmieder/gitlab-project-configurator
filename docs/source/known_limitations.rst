
Known Limitations
=================

Approvers
~~~~~~~~~

When GPC add user as approver, this feature could have no effect on the
approvers list because the user is not an **eligible** approver for Gitlab
(`see documentation
<https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#eligible-approvers>`__).

A user can be added as an approver for a project if they are a member of:

- The project.
- The project’s immediate parent group.
- A group that has access to the project via share.

Label Management
~~~~~~~~~~~~~~~~

GPC is able to add or update labels on your projects,
but it won't remove existing labels which are not part of GPC configuration.

.. note::  This is more a "wanted" bahavior than a real known limitations.

  Labels can have many usage, so we let them be defined outside of GPC.

Badges
~~~~~~

Like labels, we do not change existing badges.


External users
~~~~~~~~~~~~~~

There are some limitations when GPC add user as approver, or member,
or in protected tags or branches for external users.
GPC needs to find the user id in order to add a user to a project.
The API does not allow it for non-admin tokens.
If this external user is a member of the project,
GPC can find and add him as approver or give him rights to merge/push to protected branches.

.. note:: A workaround is to create a group with external users,
  and add this group as approvers, members, etc.
